package ru.geekbrains.gruzdovan.colmen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Elkin on 20.08.2016.
 */
public class Database_test {

    public String DB_TableName = "test1";
    public String DB_Column1 ="keywordsItems";
    public String DB_Column2 = "keywordsRelatedPersons";
    public String DB_Column3 = "pages";
    public String DB_Column4 = "rating";
    public String DB_Name = "Colmen_DB_test";
    PersonsArray personsArray;
    ArrayList<Persons> pArray;


    public ArrayList<String> itemArrayForListStatistics = new ArrayList<String>();
    Context context;
    ContentValues cv;

    public Database_test(Context context) {
        this.context = context;
    }
//    public Database_test(String DB_TableName, String DB_Column1, String DB_Column2, Context context) {
//        this.DB_TableName = DB_TableName;
//        this.DB_Column1 = DB_Column1;
//        this.DB_Column2 = DB_Column2;
//        this.context = context;
//    }
    public void initDirectoryName() {

    }
    public ArrayList<String> readDB_inArray(String spinnerPageSelected) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        pArray = new ArrayList<Persons>();

        if (pArray != null) {
            pArray.clear();
        }
//        personsArray = new PersonsArray(context);
//        itemArrayForListStatistics.clear();
        String[] columns = new String[] { DB_Column1, "sum("+DB_Column4+") as "+ DB_Column4};
//        String[] columns = new String[] { DB_Column3};
//        String[] columns = new String[] { DB_Column1,DB_Column4};
        Cursor c = db.query(DB_TableName, columns, DB_Column3+" = ?", new String[]{spinnerPageSelected}, DB_Column1, null, null);
//        Cursor c = db.query(DB_TableName, columns, DB_Column3+" = ?", new String[]{spinnerPageSelected}, null, null, null);
//        Cursor c = db.query(DB_TableName, columns, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
//            int idColIndex = c.getColumnIndex("id");
            int nameColIndex1 = c.getColumnIndex(DB_Column1);
            int nameColIndex4 = c.getColumnIndex(DB_Column4);

            do {
                Persons person = new Persons();
                person.setPerson(String.valueOf(c.getString(nameColIndex1)));
                person.setRating(String.valueOf(c.getString(nameColIndex4)));
                pArray.add(person);
//                String stringForArray = formatRating(String.valueOf(c.getString(nameColIndex1)), String.valueOf(c.getString(nameColIndex4)));
//                itemArrayForListStatistics.add(String.valueOf(c.getString(nameColIndex1)));
//                itemArrayForListStatistics.add(formatRating(String.valueOf(c.getString(nameColIndex4))));olIndex1)));
//                itemArrayForListStatistics.add(stringForArray);
//                itemArrayForListStatistics.add(String.valueOf(c.getString(nameColIndex4)));
//                Log.d("MyAPP","read array "+ String.valueOf(c.getString(nameColIndex1) + "____" + String.valueOf(c.getString(nameColIndex4))));
//                Log.d("MyAPP","read array "+ formatRating(String.valueOf(c.getString(nameColIndex4))));
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
        return itemArrayForListStatistics;
    }

    public void insertBD() {
//        доавлении новой записи в базу
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv = new ContentValues();
        cv.put(DB_Column1, "Путин");
        cv.put(DB_Column2, "Путину");
        cv.put(DB_Column3, "www.lenta.ru");
        cv.put(DB_Column4, 10);
        db.insert(DB_TableName, null, cv);

        cv = new ContentValues();
        cv.put(DB_Column1, "Путин");
        cv.put(DB_Column2, "Путинa");
        cv.put(DB_Column3, "www.ya.ru");
        cv.put(DB_Column4, 5);
        db.insert(DB_TableName, null, cv);

        cv = new ContentValues();
        cv.put(DB_Column1, "Медведев");
        cv.put(DB_Column2, "Медведеву");
        cv.put(DB_Column3, "www.lenta.ru");
        cv.put(DB_Column4, 3);
        db.insert(DB_TableName, null, cv);

        cv = new ContentValues();
        cv.put(DB_Column1, "Навальный");
        cv.put(DB_Column2, "Навальный");
        cv.put(DB_Column3, "www.lenta.ru");
        cv.put(DB_Column4, 1000);
        db.insert(DB_TableName, null, cv);


        dbHelper.close();
    }

    public String formatRating(String person, String rating) {
        int emptyLengthPartString = 40 -person.length()- rating.length();;
//        char[] chars = {'-', '-', '-', '-', '-', '-', '-', '-', '-','-'};
//        char[] chars = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' '};
        String chars = "_";
        String emptyPartString= "";
        for (int i = 0; i < emptyLengthPartString ; i++) {
            emptyPartString = emptyPartString + chars;
        }
//        String partEmptyString = new String(chars, 0, emptyLengthPartString);
        String resultString = person + emptyPartString + rating;
        Log.d("MyAPP", String.valueOf(resultString.length()));

        return resultString;

    }
//    public void deleteBD(String personName) {
//        //        уаление записи из базы
//        DBHelper dbHelper = new DBHelper(context);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
////        Log.d("bbb", DB_Column1 + "___"+personName);
//        db.delete(DB_TableName, DB_Column1+" = ?", new String[]{personName});
//        dbHelper.close();
//    }
//    public void updateBD(String itemName, String updateName) {
//        //        обновление записи в базе
//        DBHelper dbHelper = new DBHelper(context);
//        cv = new ContentValues();
//        cv.put(DB_Column1, updateName);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        db.update(DB_TableName,cv, DB_Column1+" = ?", new String[]{itemName});
//        dbHelper.close();
//    }

//    @Override
//    public void queryToBD(String columnFrom, String criterionForQuery,String argsForQuery) {
//        DBHelper dbHelper = new DBHelper(context);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        itemArrayForListStatistics.clear();
//        Cursor c = db.query(DB_TableName, new String[]{columnFrom}, criterionForQuery+" = ?", new String[]{argsForQuery}, null, null, null);
//        if (c.moveToFirst()) {
//
//            // определяем номера столбцов по имени в выборке
//            int nameColIndex = c.getColumnIndex(columnFrom);
////            int namePersonsColIndex = c.getColumnIndex(DB_Column2);
//            do {
//                itemArrayForListStatistics.add(String.valueOf(c.getString(nameColIndex)));
//                Log.d("MyAPP","queryDB "+ String.valueOf(c.getString(nameColIndex)));
////                Log.d("MyAPP", String.valueOf(c.getString(nameColIndex) + "____" + String.valueOf(c.getString(namePersonsColIndex))));
//            } while (c.moveToNext());
//        }
//        c.close();
//        dbHelper.close();
//    }


    public boolean checkValueItemInDB(String personValue) {
        //     проверка на дублирование в базе значения вводимой Личности

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DB_TableName, null, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex(DB_Column1);
//            int emailColIndex = c.getColumnIndex(DB_Column2);

            do {
//                сравнение каждой записи таблицы в БД с введенным в диалоге значением, в дальнейшем доработать, как проверка через запрос
                if (c.getString(nameColIndex).equals(personValue)) {
                    return true;
                }
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
        return false;
    }
    class DBHelper extends SQLiteOpenHelper {


        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, DB_Name, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("create table " + DB_TableName + " ("
                    + "id integer primary key autoincrement,"
                    +  DB_Column1 + " text,"+
                    DB_Column2+ " text,"+
                   DB_Column3 + " text,"+
                   DB_Column4 + " text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }


}

}
