package ru.geekbrains.gruzdovan.colmen;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class DirectoryFragment extends ListFragment {
    public ArrayList<String> directoryArray = new ArrayList<String>();//массив для формирования справочника

    static interface DirectoryListener {
        void itemClickedDirPersons(String position);
    }

    private DirectoryFragment.DirectoryListener DirectoryListener;

    public DirectoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.directory_fragment, null);
        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.DirectoryListener = (DirectoryFragment.DirectoryListener) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //  загрузка значений из базы
        ListView lvDir = getListView();
        lvDir.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);//выбор способа выдклени ListView
//        добавление заголовка справочника
//        View headerLV = getActivity().getLayoutInflater().inflate(R.layout.header_directory_fragment, null);
//        lvDir.addHeaderView(headerLV);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        try {
            if (DirectoryListener != null) {
                String str = l.getItemAtPosition(position).toString();
                DirectoryListener.itemClickedDirPersons(str);
            }
        } catch (NullPointerException e){
            DirectoryListener = null;
        }

    }

}
