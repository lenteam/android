package ru.geekbrains.gruzdovan.colmen;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StatisticFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class StatisticFragment extends ListFragment {

    private OnFragmentInteractionListener mListener;
    String getSpinnerValPage;

    public StatisticFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Bundle mArgs = getArguments();
//        getSpinnerValPage = mArgs.getString(getResources().getString((R.string.bundleFieldBD_Name3)));
//        Database_test database_test = new Database_test(getActivity());
//        database_test.readDB_inArray(getSpinnerValPage);
//        database_test.insertBD();

//        GridView gridView = (GridView) greedInflater.findViewById(R.id.grid_view);
//        gridView.setAdapter(new StatisticAdapter(getActivity(),database_test.itemArrayForListStatistics));
//        CrimeAdapter adapter = new CrimeAdapter(database_test.pArray);
//        setListAdapter(adapter);
//        getSpinnerValPage = mArgs.getString(String.valueOf(R.string.bundleFieldBD_Name3));
//        mStr = getArguments() != null ? getArguments().getInt("num") : 1;

    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View greedInflater = inflater.inflate(R.layout.greed_item, container, false);
//
//        // Inflate the layout for this fragment
////        Bundle mArgs = getArguments();
////        String getSpinnerValPage = mArgs.getString(String.valueOf(R.string.bundleFieldBD_Name3));
//        Database_test database_test = new Database_test(getActivity());
//        database_test.readDB_inArray(getSpinnerValPage);
//        database_test.insertBD();
//
////        GridView gridView = (GridView) greedInflater.findViewById(R.id.grid_view);
////        gridView.setAdapter(new StatisticAdapter(getActivity(),database_test.itemArrayForListStatistics));
//        CrimeAdapter adapter = new CrimeAdapter(database_test.pArray);
//        setListAdapter(adapter);
//
//        return greedInflater;
//    }

//    private class CrimeAdapter extends ArrayAdapter<Persons> {
//        public CrimeAdapter(ArrayList<Persons> person) {
//            super(getActivity(), 0, person);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                convertView = getActivity().getLayoutInflater()
//                        .inflate(R.layout.list_item_persons_rating, null);
//            }
//            Persons person = getItem(position);
//            TextView titleTextView = (TextView) convertView.findViewById(R.id.persons_list_item);
//            titleTextView.setText(person.getPerson());
//            TextView dateTextView = (TextView) convertView.findViewById(R.id.rating_list_item);
//            dateTextView.setText(person.getRating());
//            return convertView;
//        }
//
//    }


    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        Bundle mArgs = getArguments();
//        getSpinnerValPage = mArgs.getString(String.valueOf(R.string.bundleFieldBD_Name3));
////        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
////        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement OnFragmentInteractionListener");
////        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
