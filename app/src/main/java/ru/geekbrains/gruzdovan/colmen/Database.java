package ru.geekbrains.gruzdovan.colmen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Elkin on 20.08.2016.
 */
public class Database implements Repository {

    public String DB_TableName;
    public String DB_Column1;
    public String DB_Column2;
    public String DB_Name = "Colmen_DB";


    public ArrayList<String> directoryArray = new ArrayList<String>();
    Context context;
    ContentValues cv;

    public Database(String DB_TableName, String DB_Column1, String DB_Column2, Context context) {
        this.DB_TableName = DB_TableName;
        this.DB_Column1 = DB_Column1;
        this.DB_Column2 = DB_Column2;
        this.context = context;
    }
    public void initDirectoryName() {

    }

    public void insertBD(String personName,String spinnerValue) {
//        доавлении новой записи в базу
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv = new ContentValues();
        cv.put(DB_Column1, personName);
        if (spinnerValue != null) {
            cv.put(DB_Column2, spinnerValue);
        }
        Log.d("MyAPP","insert in DB: "+ personName+"____" + spinnerValue);
        db.insert(DB_TableName, null, cv);
        dbHelper.close();
    }
    public void deleteBD(String personName) {
        //        уаление записи из базы
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        Log.d("bbb", DB_Column1 + "___"+personName);
        db.delete(DB_TableName, DB_Column1+" = ?", new String[]{personName});
        dbHelper.close();
    }
    public void updateBD(String itemName, String updateName) {
        //        обновление записи в базе
        DBHelper dbHelper = new DBHelper(context);
        cv = new ContentValues();
        cv.put(DB_Column1, updateName);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.update(DB_TableName,cv, DB_Column1+" = ?", new String[]{itemName});
        dbHelper.close();
    }

    @Override
    public void queryToBD(String columnFrom, String criterionForQuery,String argsForQuery) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        directoryArray.clear();
        Cursor c = db.query(DB_TableName, new String[]{columnFrom}, criterionForQuery+" = ?", new String[]{argsForQuery}, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int nameColIndex = c.getColumnIndex(columnFrom);
//            int namePersonsColIndex = c.getColumnIndex(DB_Column2);
            do {
                directoryArray.add(String.valueOf(c.getString(nameColIndex)));
//                Log.d("MyAPP","queryDB "+ String.valueOf(c.getString(nameColIndex)));
//                Log.d("MyAPP", String.valueOf(c.getString(nameColIndex) + "____" + String.valueOf(c.getString(namePersonsColIndex))));
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
    }

    public ArrayList<String> readDB_inArray() {
//        чтение поля "name" базы данных в АррейЛист для формирования стправочника Личности
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        directoryArray.clear();
        Cursor c = db.query(DB_TableName, null, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex(DB_Column1);
            if (DB_Column2 != null) {
                int namePersonsColIndex = c.getColumnIndex(DB_Column2);
            }

            do {

                directoryArray.add(String.valueOf(c.getString(nameColIndex)));
//                Log.d("MyAPP","read array "+ String.valueOf(c.getString(nameColIndex) + "____" + String.valueOf(c.getString(namePersonsColIndex))));
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
        return directoryArray;
    }
    public boolean checkValueItemInDB(String personValue) {
        //     проверка на дублирование в базе значения вводимой Личности

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DB_TableName, null, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex(DB_Column1);
//            int emailColIndex = c.getColumnIndex(DB_Column2);

            do {
//                сравнение каждой записи таблицы в БД с введенным в диалоге значением, в дальнейшем доработать, как проверка через запрос
                if (c.getString(nameColIndex).equals(personValue)) {
                    return true;
                }
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
        return false;
    }
    class DBHelper extends SQLiteOpenHelper {


        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, DB_Name, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // создаем таблицу для справочника Личности
            db.execSQL("create table " + context.getResources().getString(R.string.db_table_persons) + " ("
                    + "id integer primary key autoincrement,"
                    +  context.getResources().getString(R.string.db_column_persons)+" text);");
            // создаем таблицу для справочника Ключевые слова, с дополнительным полем, связанным с таблицей Личности
            db.execSQL("create table " + context.getResources().getString(R.string.db_table_keywords) + " ("
                    + "id integer primary key autoincrement,"
                    +  context.getResources().getString(R.string.db_column_keywords) + " text,"
                    +  context.getResources().getString(R.string.db_related_column_keywords) + " text" + ");");
            // создаем таблицу для справочника Сайты
            db.execSQL("create table " + context.getResources().getString(R.string.db_table_pages) + " ("
                    + "id integer primary key autoincrement,"
                    +  context.getResources().getString(R.string.db_column_pages)+" text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }


}

}
