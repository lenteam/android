package ru.geekbrains.gruzdovan.colmen;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Elkin on 05.09.2016.
 */
public class StatisticAdapter extends BaseAdapter {
    private Context context;
    private final ArrayList<String> mThumbIds;
//    private final String[] mThumbIds;

    public StatisticAdapter(Context context, ArrayList<String> mThumbIds) {
        this.context = context;
        this.mThumbIds = mThumbIds;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.item_for_greed_table, null);

            // set image based on selected text
//            ImageView imageView = (ImageView) gridView
//                    .findViewById(R.id.grid_item_image);
            TextView tvLbl = (TextView) gridView.findViewById(R.id.label);
            tvLbl.setText(mThumbIds.get(position));
//            Typeface typeFace = Typefaces.get(context, "fonts/Roboto-Regular.ttf");
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
            tvLbl.setTypeface(typeFace);


//            imageView.setImageResource(Integer.parseInt(mThumbIds[position]));


        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

    @Override
    public int getCount() {
        return mThumbIds.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}