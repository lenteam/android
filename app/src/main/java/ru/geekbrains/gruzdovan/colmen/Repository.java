package ru.geekbrains.gruzdovan.colmen;

import java.util.ArrayList;

/**
 * Created by Elkin on 25.08.2016.
 */
public interface Repository {
    void insertBD(String personName,String spinnerValue);

    void deleteBD(String personName);

    void updateBD(String itemName, String updateName);

    void queryToBD(String columnFrom, String criterionForQuery, String argsForQuery);

    ArrayList<String> readDB_inArray();

    boolean checkValueItemInDB(String personValue);


}
