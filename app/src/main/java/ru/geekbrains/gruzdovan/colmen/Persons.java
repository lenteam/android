package ru.geekbrains.gruzdovan.colmen;

/**
 * Created by Elkin on 10.09.2016.
 */
public class Persons {
    String person;
    String rating;

    public void setPerson(String person) {
        this.person = person;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPerson() {

        return person;
    }

    public String getRating() {
        return rating;
    }

}
