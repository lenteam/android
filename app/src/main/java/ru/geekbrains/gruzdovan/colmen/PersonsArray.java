package ru.geekbrains.gruzdovan.colmen;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Elkin on 10.09.2016.
 */
public class PersonsArray {
    private ArrayList<Persons> arrayPersons;
    private static PersonsArray cPersonsArray;
    private Context mAppContext;

    public PersonsArray(Context appContext) {
        mAppContext = appContext;
        arrayPersons = new ArrayList<Persons>();

    }

    public ArrayList<Persons> getmPersonsArray() {
        return arrayPersons;
    }

    public static PersonsArray get(Context context) {
        if (cPersonsArray == null) {
            cPersonsArray = new PersonsArray(context.getApplicationContext());
        }
        return cPersonsArray;
    }

}
