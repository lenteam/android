package ru.geekbrains.gruzdovan.colmen;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;


/**
 * Created by Elkin on 09.08.2016.
 */
public class DirectoryActivity extends AppCompatActivity implements DirectoryFragment.DirectoryListener,AddElementsDialogInDirectory.AddElemDialogListener {
    String btnName;
    String itemSelectedDirPersons;
    FragmentTransaction ft;
    public static final String EXTRA_DIRECTORY_NAME = "EXTRA_DIRECTORY_NAME";
    Button btnDelete;
    Button btnUpdate;
    Button btnAdd;
    Database database;
    String directoryName;
    Boolean isDirectoryKeywords;
    Bundle args;

    //    private String DB_Column1;
    private String DB_TableName;
    private String DB_Column1;
    private String DB_Column2;
    Spinner spinner;
    String spinnerSelectPosition;
//    private ArrayList<String> directoryArray = new ArrayList<String>();//массив для формирования справочника


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.directory_activity);
        directoryName = (String) getIntent().getExtras().get(EXTRA_DIRECTORY_NAME);//название выбрвнного справочника
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        disableBtnDeleteAndUpdate();
        //      в случае выбора справочника "Ключевые слова" учитываются данные из справвочника "Персоны"
        //то есть при выборе опрделенной личности из Spinner отображаются ключевые слова только по этой дичности
//        spinnerSelectPosition = "";
        if (directoryName.equals(getResources().getString(R.string.keywords)))  {
            isDirectoryKeywords = true;
            spinnerForDirectoryKeywordsUpdate();
//            if (database.itemArrayForListStatistics.size() != 0) {
//                spinnerSelectPosition = String.valueOf(spinner.getItemAtPosition(0));
        } else {
            isDirectoryKeywords = false;
        }
        updateDirectoryFragment();
        this.setTitle(getResources().getString(R.string.directory) + " \""+directoryName+"\"");


    }
    private void updateDirectoryFragment() {

        initDB();//инициация базы данных для формирования аррея для выбранного справочника
        DirectoryFragment directoryFragment = new DirectoryFragment();
//        чтение базы данных справочника и загрузка сохраненных данных
//        directoryArray.addAll(database.itemArrayForListStatistics);
        if (isDirectoryKeywords == true) {//формирование массива данных для справочника без учета заачения Spinner
//            spinnerForDirectoryKeywordsUpdate();
            try {
                database.queryToBD(DB_Column1, DB_Column2, spinnerSelectPosition);
            } catch (Exception e) {
                Log.d("MyAPP", e.toString());
            }
//            if (database.itemArrayForListStatistics.size() > 0){
//                database.queryToBD(DB_Column1, DB_Column2, spinnerSelectPosition);
//            }
        } else {
            database.readDB_inArray();
        }
//        View headerLV = getLayoutInflater().inflate(R.layout.header_directory_fragment, null);
//        directoryFragment.getListView().addHeaderView(headerLV);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_activated_1, database.directoryArray
        );

        //если справочник пустой, то отключить кнопки Удалить и Добавить
        if (database.directoryArray.size() == 0) {
            disableBtnDeleteAndUpdate();
        }
        directoryFragment.setListAdapter(arrayAdapter);
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.rlDirectoryFragment, directoryFragment);
        ft.commit();

    }
    public void initDB() {
//        DB_Column2 = "DirectoryNameTech";
        if (directoryName.equals(getResources().getString(R.string.persons))) {
            DB_TableName = getResources().getString(R.string.db_table_persons);
            DB_Column1 = getResources().getString(R.string.db_column_persons);
        } else if (directoryName.equals(getResources().getString(R.string.keywords))) {
            DB_TableName = getResources().getString(R.string.db_table_keywords);
            DB_Column1 = getResources().getString(R.string.db_column_keywords);
            DB_Column2 = getResources().getString(R.string.db_related_column_keywords);
        } else if (directoryName.equals(getResources().getString(R.string.pages))) {
            DB_TableName = getResources().getString(R.string.db_table_pages);
            DB_Column1 = getResources().getString(R.string.db_column_pages);
        }
        database = new Database(DB_TableName, DB_Column1, DB_Column2, getApplicationContext());

    }

    private void spinnerForDirectoryKeywordsUpdate() {

//         контейнер для хранения Спиннера, по умолчанию скрытый
        LinearLayout llSpinnerTextViewForPages = (LinearLayout) findViewById(R.id.llSpinnerTextViewForPages);
        spinner = (Spinner)findViewById(R.id.spinnerDirectoryPersons);
        llSpinnerTextViewForPages.setVisibility(View.VISIBLE);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    spinnerSelectPosition = (String) spinner.getSelectedItem();
                    updateDirectoryFragment();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
//         инициацияия переменной с названием справочника Личности, который является источником данных для формирования спиннеера
        String DirectoryPersonName = getResources().getString(R.string.db_table_persons);

        //инициация базы данных для справочника Личности
//        DB_Column2 = "DirectoryNameTech";
        database = new Database(DirectoryPersonName, getResources().getString(R.string.db_column_persons),DB_Column2, getApplicationContext());
        database.readDB_inArray();
        if (database.directoryArray.size() > 0) {

            btnAdd.setEnabled(true);
            // Настраиваем адаптер
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this, android.R.layout.simple_list_item_activated_1, database.directoryArray
            );
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // Вызываем адаптер
            spinner.setAdapter(arrayAdapter);
            if (spinnerSelectPosition == null) {
                spinnerSelectPosition = database.directoryArray.get(0);
            }

        } else {
            btnAdd.setEnabled(false);
        }
            initDB();//воставновить параметра быза данных для текущего справочника
    }


    public void onClickDirectoryPersons(View view) {
//        отправка названия кнопки в DialogFragment
        args = new Bundle();
        AddElementsDialogInDirectory addElemDialog = new AddElementsDialogInDirectory();
        args.putString(getString(R.string.bundleDirectoryName), DB_TableName);
        args.putString(getString(R.string.bundleFieldBD_Name1), DB_Column1);
        args.putString(getString(R.string.bundleFieldBD_Name2), DB_Column2);

        switch (view.getId()) {
            case R.id.btnAdd:
                btnName = getResources().getString(R.string.btn_dialog_save);
//                передача названия кнопки в диалог
                args.putString(getString(R.string.bundleBtnName), btnName);
                break;
            case R.id.btnUpdate:
                btnName = getResources().getString(R.string.btn_dialog_update);
                //передача названия кнопки и позиции выделенного элемента в диалог
                args.putString(getString(R.string.bundleBtnName), btnName);
                args.putString(getString(R.string.keyElementName), itemSelectedDirPersons);

                break;
            case R.id.btnDelete:
                btnName = getResources().getString(R.string.btn_dialog_delete);
                //передача названия кнопки и позиции выделенного элемента в диалог
                args.putString(getString(R.string.bundleBtnName), btnName);
                args.putString(getString(R.string.keyElementName), itemSelectedDirPersons);

                break;

        }
        addElemDialog.setArguments(args);
        addElemDialog.show(getSupportFragmentManager(),"1");
    }

    @Override
    public void dialogButtonClicked(boolean isClicked, String valueView) {
//        возврат названия нажатой кнопки от DialogFragment в случае нажатия в диалоге ОК
        if (isClicked) {

            if (btnName.equals(getResources().getString(R.string.btn_dialog_save))) {
                database.insertBD(valueView, spinnerSelectPosition);
            } else if (btnName.equals(getResources().getString(R.string.btn_dialog_delete))) {
                database.deleteBD(itemSelectedDirPersons);
            } else if (btnName.equals(getResources().getString(R.string.btn_dialog_update))) {
                database.updateBD(itemSelectedDirPersons,valueView);
            }
            //обновление справоника с учетом изменений
            updateDirectoryFragment();
        }
    }

    @Override
    public void itemClickedDirPersons(String position) {
//        получение позиции выделенного элемента из DirectoryFragment
        if (position != null) {
            itemSelectedDirPersons = position;
            //сделать неактивными кнопки Удалить и Редакатировать в случае, если ни одна позиция в справочнике не выделена
            enableBtnDeleteAndUpdate();
        }
    }

    private void enableBtnDeleteAndUpdate() {
        btnDelete.setEnabled(true);
        btnUpdate.setEnabled(true);
    }
    private void disableBtnDeleteAndUpdate() {
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
    }
}



