package ru.geekbrains.gruzdovan.colmen;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AddElementsDialogInDirectory extends DialogFragment implements View.OnClickListener {
    View dialogInflater;
    EditText etNewElement;
    Database database;
    LinearLayout llPersonsDialog;
    String itemSelectedDirPersons;
    String btnName;
    String DB_TableName;
    String DB_Column1;
    String DB_Column2;


    static interface AddElemDialogListener {
        void dialogButtonClicked(boolean isClicked, String nameView);
    }

    private AddElemDialogListener DialogListener;

    FragmentTransaction ft;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.DialogListener = (AddElemDialogListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        DirectoryActivity directoryActivity = new DirectoryActivity();

        dialogInflater = inflater.inflate(R.layout.add_new_element_dialog_directory, container, false);
        //  получение информации об имени нажатой кнопки в DialogActivity и выделенном элементе в DirectoryFragment
        Bundle mArgs = getArguments();
        btnName = mArgs.getString(getString(R.string.bundleBtnName));//отлов нажатой кнопки в DirectoryActivity
        itemSelectedDirPersons = mArgs.getString(getString(R.string.keyElementName));
        llPersonsDialog = (LinearLayout) dialogInflater.findViewById(R.id.llPersonsDialog);
        if (btnName == getResources().getString(R.string.btn_dialog_save)) {
//            в DirectoryActivity нажата кнопка "Добавить"
            viewKeyboardOnCreateDialog(true);
            insertEditTextPersonName();
            getDialog().setTitle(R.string.title_dialog_new_element);
        } else if (btnName == getResources().getString(R.string.btn_dialog_update)) {
//            в DirectoryActivity нажата кнопка "Добавить"
            insertTextViewNameElementDeleted();
            getDialog().setTitle(R.string.title_dialog_upd_element);
        } else if (btnName == getResources().getString(R.string.btn_dialog_delete)) {
//            в DirectoryActivity нажата кнопка "Удалить"
            insertTextViewNameElementUpdated();

        }

        dialogInflater.findViewById(R.id.btnSaveElementDirectoryPersons).setOnClickListener(this);
        dialogInflater.findViewById(R.id.btnCancelElementDirectoryPersons).setOnClickListener(this);

        return dialogInflater;

    }

    private void insertTextViewNameElementUpdated() {
        Button btnSave = (Button) dialogInflater.findViewById(R.id.btnSaveElementDirectoryPersons);
        btnSave.setText(getResources().getString(R.string.btn_dialog_yes));
        Button btnCancel = (Button) dialogInflater.findViewById(R.id.btnCancelElementDirectoryPersons);
        btnCancel.setText(getResources().getString(R.string.btn_dialog_no));
        getDialog().setTitle(R.string.title_dialog_del_element);
        TextView tvDialogInfo = new TextView(getActivity());
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params3.gravity = Gravity.CENTER;
        llPersonsDialog.addView(tvDialogInfo, params3);
        tvDialogInfo.setText(getString(R.string.text_dialog_delete) +  " \""+itemSelectedDirPersons + "\"?");
        tvDialogInfo.setTextSize(14);
    }

    private void insertTextViewNameElementDeleted() {
        etNewElement = new EditText(getActivity());
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params2.gravity = Gravity.CENTER;
        llPersonsDialog.addView(etNewElement, params2);
        etNewElement.setLayoutParams(params2);
        etNewElement.setText(itemSelectedDirPersons);
        etNewElement.setWidth(800);
    }

    private void insertEditTextPersonName() {

        etNewElement = new EditText(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        llPersonsDialog.addView(etNewElement, params);
        etNewElement.setLayoutParams(params);
        etNewElement.setHint(R.string.strHintEdText);
        etNewElement.setWidth(800);
    }

    private void viewKeyboardOnCreateDialog(Boolean isShowKeyboard) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (isShowKeyboard) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } else {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSaveElementDirectoryPersons:
                if (etNewElement != null) {//проверка введенных данных дилогового окна (EditText)
                    String etNewElementString = etNewElement.getText().toString();
                    initBD();//инициация базы данных для проверки совпадения введенного значнеия с уже сущетсвующими данными в базе
//                    проверка введенного значения на то, чтобы оно не было пустым
                    if (etNewElementString.trim().length() == 0) {
                        etNewElement.setError(getString(R.string.stringEmptyValue));
//                    проверка введенного значения на то, чтобы оно не было пустым
                    } else if (!isValidDirectoryValue(etNewElementString)&&DB_TableName.equals(getResources().getString(R.string.db_table_pages))) {
                        etNewElement.setError(getString(R.string.str_error_only_pages));
                    } else if (!isValidDirectoryValue(etNewElementString)) {
                        etNewElement.setError(getString(R.string.str_error_only_cyrillic));
                    } else if (database.checkValueItemInDB(etNewElementString)) {
                        etNewElement.setError(getString(R.string.str_error_duplication_value));
                    } else {
                        if (DialogListener != null) {
                            DialogListener.dialogButtonClicked(true, etNewElementString.trim());
                        }
                        dismiss();

                        viewKeyboardOnCreateDialog(false);
                    }
                } else {
                    if (DialogListener != null) {
                        DialogListener.dialogButtonClicked(true,"Nothing");
                    }
                    dismiss();
                    viewKeyboardOnCreateDialog(false);

                }
                break;
            case R.id.btnCancelElementDirectoryPersons:
                Toast.makeText(getActivity(), R.string.not_saved_element, Toast.LENGTH_SHORT).show();
                dismiss();
                viewKeyboardOnCreateDialog(false);
                break;
        }

    }
    private void initBD() {
        Bundle mArgs = getArguments();
        DB_TableName = mArgs.getString(getString(R.string.bundleDirectoryName));
        DB_Column1 = mArgs.getString(getString(R.string.bundleFieldBD_Name1));
        DB_Column2 = mArgs.getString(getString(R.string.bundleFieldBD_Name2));
        database = new Database(DB_TableName, DB_Column1,DB_Column2, getActivity().getApplicationContext());
    }
    private boolean isValidDirectoryValue(String strDirValue) {
//        проверка, в завсимости от вправочника
        if (DB_TableName.equals(getResources().getString(R.string.db_table_pages))) {//справочник "Сайты"
            return android.util.Patterns.WEB_URL.matcher(strDirValue).matches();
        } else {
            String PERSONS_PATTERN = "^[А-яЁё]{1,60}$";
            Pattern pattern = Pattern.compile(PERSONS_PATTERN);
            Matcher matcher = pattern.matcher(strDirValue);
            return matcher.matches();
        }
    }

}
