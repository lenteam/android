package ru.geekbrains.gruzdovan.colmen;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class StatisticsActivity extends AppCompatActivity {

    public static final String EXTRA_DIRECTORY_NAME = "EXTRA_DIRECTORY_NAME";
    Spinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        String directoryName = (String) getIntent().getExtras().get(EXTRA_DIRECTORY_NAME);//название выбрвнного справочника
        this.setTitle(directoryName);

        TextView tvLabelName = (TextView) findViewById(R.id.tvLabelName);
        TextView tvLabelNumber = (TextView) findViewById(R.id.tvLabelNumber);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        tvLabelName.setTypeface(typeFace);
        tvLabelNumber.setTypeface(typeFace);
//        loadStatisticFragment();
        spinnerForPagesKeywordsUpdate();

    }

    private void loadStatisticFragment() {
//        Bundle args = new Bundle();
        String spinnerSelectedPage = (String) spinner.getSelectedItem();
//        args.putString(getString(R.string.bundleFieldBD_Name3), spinnerSelectedPage);
        StatisticFragment statisticFragment = new StatisticFragment();
//        statisticFragment.setArguments(args);
        Database_test database_test = new Database_test(this);
        database_test.readDB_inArray(spinnerSelectedPage);
        database_test.insertBD();
        FragmentTransaction ft;
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.greedContainer, statisticFragment);
        CrimeAdapter adapter = new CrimeAdapter(database_test.pArray);
        statisticFragment.setListAdapter(adapter);
        ft.commit();

    }
    private class CrimeAdapter extends ArrayAdapter<Persons> {
        public CrimeAdapter(ArrayList<Persons> person) {
            super(getApplicationContext(), 0, person);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater()
                        .inflate(R.layout.list_item_persons_rating, null);
            }
            Persons person = getItem(position);
            TextView personsTextView = (TextView) convertView.findViewById(R.id.persons_list_item);
            personsTextView.setText(person.getPerson());
            TextView ratingTextView = (TextView) convertView.findViewById(R.id.rating_list_item);
            ratingTextView.setText(person.getRating());
            Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            personsTextView.setTypeface(typeFace);
            ratingTextView.setTypeface(typeFace);
            return convertView;
        }

    }

    private void spinnerForPagesKeywordsUpdate() {
        spinner = (Spinner)findViewById(R.id.spinnerPagesNames);

        //инициация базы данных для отчета Общая статистика
        Database database = new Database(getResources().getString(R.string.db_table_pages), getResources().getString(R.string.db_column_pages),"", getApplicationContext());
        database.readDB_inArray();
        if (database.directoryArray.size() > 0) {

//            btnAdd.setEnabled(true);
            // Настраиваем адаптер
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this, android.R.layout.simple_list_item_activated_1, database.directoryArray
            );
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//             Вызываем адаптер
            spinner.setAdapter(arrayAdapter);

//            if (spinnerSelectPosition == null) {
//                spinnerSelectPosition = database.directoryArray.get(0);
//            }
//
//        } else {
//            btnAdd.setEnabled(false);
        }
    }

    public void onClickApply(View view) {
        loadStatisticFragment();
    }
}